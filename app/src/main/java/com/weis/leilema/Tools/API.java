package com.weis.leilema.Tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * This file is write about all API this app in need and some get value method.
 *
 * Created by lbstek on 2017/3/22.
 */

public class API {

    private final static boolean isDebug = true;

    public static String debugServerURL = "http://www.areyoutired.weis-cmg.com/";
    public static String productionServerURL = "";
    //public static String debugServerURL = "http://192.168.1.114:8000";
    public static String versionControlURL = "http://vcm.lbstek.com/fqnd/areyoutired/android/";
    /** Membership Terms of Use */
    public static String membership = "https://www.google.com.tw/";
    /** Personal Data Protection Policy */
    public static String personalDataProtectionPolicy = "https://tw.yahoo.com/";

    public static String REGISTER = "/api/member/";
    public static String LOGIN = "/api/member/login/";
    public static String INQUIRY_RESULT = "/api/inquiry/result/";
    public static String INQUTRY_FEEDBACK = "/api/inquiry/feedback/";



    public static String getServerURL() {
        if (API.isDebug) {
            return API.debugServerURL;
        } else {
            SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
            return authData.getString(App.PRODUCTION_SERVER_URL, "");
        }
    }

    public static String getSelfUserToken(){
        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
        return authData.getString(App.USER_TOKEN, "");
    }

    public static String getJWTToken() {
        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
        return "JWT " + authData.getString(App.TOKEN, "");
    }
//
//    public static String getSelfUserID(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getString(App.USER_ID, "0");
//    }
//
//    public static int getAgeStart(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getInt(App.AGE_START, 18);
//    }
//
//    public static int getAgeTo(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getInt(App.AGE_TO, 100);
//    }
//
//    public static int getIntestedInSex(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getInt(App.INTERSTED_IN_SEX, 0);
//    }
//
//    public static int getDistance(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getInt(App.DISTANCE, 5);
//    }
//
//    public static String getUserMugshotUrl(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getString(App.MUGSHOT, "");
//    }
//
//    public static String getLatitude(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getString(App.LATITUDE, "25.0512067");
//    }
//
//    public static String getLongitude(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getString(App.LONGITUDE, "121.5172482");
//    }
//
//    public static String getPushId(){
//        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, Context.MODE_PRIVATE);
//        return authData.getString(App.BAIDU_PUSH_ID, "");
//    }
}
