package com.weis.leilema.Tools;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;

import java.util.UUID;

/**
 *
 *
 * Created by lbstek on 2017/3/22.
 */

public class App extends Application {
    public static final String USER_ACCOUNT_DATA = "user_account_data";
    public static final String DEVICE_ID = "device_id";
    public static final String LOGIN_DATA = "login_data";

    /** public context, often used in obtaining specific String. */
    public static Context context;

    public static final String PRODUCTION_SERVER_URL = "productionServerURL";
    public static final String AUTH_DATA = "auth_data";
    public static final String TOKEN = "token";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_ID = "user_id";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static String getDeviceId() {

        String device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (device_id == null || device_id.equals("")) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            device_id = wifiInfo.getMacAddress();
            if (device_id == null || device_id.equals("")) {
                SharedPreferences settings = context.getSharedPreferences(USER_ACCOUNT_DATA, 0);
                device_id = settings.getString(DEVICE_ID, "");
                if (device_id.equals("")) {
                    device_id = UUID.randomUUID().toString();
                    settings.edit().putString(DEVICE_ID, device_id).apply();
                }
            }
            if (device_id.equals("")) {
                device_id = "Android id is empty";
            }
        }
        return device_id;
    }
}
