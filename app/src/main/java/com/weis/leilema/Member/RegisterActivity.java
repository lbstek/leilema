package com.weis.leilema.Member;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.weis.leilema.R;
import com.weis.leilema.Tools.API;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    private String TAG = this.getClass().getSimpleName();

    @BindView(R.id.button)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        AndroidNetworking.initialize(getApplicationContext());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

    }

    void register() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("email", "jackchu@lbstek.com");
            jsonRequest.put("password", "123456");
            jsonRequest.put("password2", "123456");
            jsonRequest.put("gender", "1");
            jsonRequest.put("blood_type", "4");
            jsonRequest.put("birthday", "1992-03-05");
            jsonRequest.put("education", "3");
            jsonRequest.put("income", 2);
            jsonRequest.put("job", 5);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.d(TAG, "url: " + API.getServerURL() + API.REGISTER);

        AndroidNetworking.post(API.getServerURL() + API.REGISTER)
                .addJSONObjectBody(jsonRequest)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response:" + response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getResponse());
                    }
                });
    }
}
