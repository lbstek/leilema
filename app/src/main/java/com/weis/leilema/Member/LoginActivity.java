package com.weis.leilema.Member;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.weis.leilema.MainActivity;
import com.weis.leilema.R;
import com.weis.leilema.Tools.API;
import com.weis.leilema.Tools.App;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private String TAG = this.getClass().getSimpleName();

    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    void login(){
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("email", "jackchu@lbstek.com");
            jsonRequest.put("password", "123456");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.d(TAG, "url: " + API.getServerURL() + API.LOGIN);

        AndroidNetworking.post(API.getServerURL() + API.LOGIN)
                .addJSONObjectBody(jsonRequest)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response:" + response);
                        // 儲存使用者資訊
                        try {
                            JSONObject data = new JSONObject(response).getJSONObject("data");
                            SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, MODE_PRIVATE);
                            authData.edit().putString(App.LOGIN_DATA, data.toString()).apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getResponse());
                    }
                });
    }
}