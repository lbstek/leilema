package com.weis.leilema.Member;

/**
 * JavaBean for login, used to save user's data.
 *
 * Created by lbstek on 2017/3/30.
 */

public class UserBean {
    /**
     * email : jackchu@lbstek.com
     * user : 28
     * jwt_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImphY2tjaHVAbGJzdGVrLmNvbSIsImV4cCI6MTQ5MDg2Mzc1NywidXNlcm5hbWUiOiJqYWNrY2h1QGxic3Rlay5jb20iLCJ1c2VyX2lkIjoyOH0.BHMpU3aTkO1iz_OSbbIGmncIXcMUi_FuSYPSXkWbTM4
     */

    private String email;
    private int user;
    private String jwt_token;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }
}
