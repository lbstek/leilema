package com.weis.leilema;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.weis.leilema.Member.LoginActivity;
import com.weis.leilema.Tools.API;
import com.weis.leilema.Tools.App;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        versionCheck();
    }

    // 檢查使用者版本
    void versionCheck() {
        // 取得版本資訊
        PackageInfo pkgInfo = null;
        try {
            pkgInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int verCode = pkgInfo.versionCode;
        String verName = pkgInfo.versionName;
        //Log.d(TAG, "verCode:" + verCode + " verName" + verName);

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("version", verCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(API.versionControlURL)
                .addJSONObjectBody(jsonRequest)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response:" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String url = jsonObject.getJSONObject("data").getString("FQND");
                            SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, MODE_PRIVATE);
                            authData.edit().putString(App.PRODUCTION_SERVER_URL, url).apply();
                            Log.d(TAG, "Production server URL:　" + authData.getString(App.PRODUCTION_SERVER_URL, ""));

                            // 通過版本控制檢查,看之後討論的基制決定做法
                            //Log.d(TAG, API.getJWTToken());
//                            if (API.getJWTToken().equals("JWT ")){
//                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//                            }else {
//                                startActivity(new Intent(SplashActivity.this, MainActivity.class));
//                            }
//
                            //檢查通過進入下一頁
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getResponse());
                    }
                });

    }
}
