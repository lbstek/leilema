package com.weis.leilema;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.weis.leilema.Member.UserBean;
import com.weis.leilema.Tools.API;
import com.weis.leilema.Tools.App;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_result)
    Button btnResult;
    @BindView(R.id.btn_feedback)
    Button btnFeedback;

    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inquiry_result();
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inquiry_feedback();
            }
        });
    }

    void inquiry_result() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("type", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "url: " + API.getServerURL() + API.INQUIRY_RESULT);

        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, MODE_PRIVATE);
        Gson gson = new Gson();
        UserBean userBean = gson.fromJson(authData.getString(App.LOGIN_DATA, ""), UserBean.class);
        Log.d(TAG, "onResponse: " + userBean.getJwt_token());

        AndroidNetworking.post(API.getServerURL() + API.INQUIRY_RESULT)
                .addHeaders("Authorization", "JWT " + userBean.getJwt_token())
                .addJSONObjectBody(jsonRequest)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response:" + response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getResponse());
                    }
                });
    }


    // 目前僅完成測試傳接,之後要把"inquiry"這個參數帶入前面的
    void inquiry_feedback() {
        JSONObject jsonRequest = new JSONObject();
        try {
            //
            jsonRequest.put("inquiry", "16");
            // score 0:非常不準確 1:有點不準 2:普通 3:還挺準的 4:非常準確
            jsonRequest.put("score", "3");
            // 讓使用者填寫文字回饋(可以是空值)
            jsonRequest.put("context", "asdasdasd");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "url: " + API.debugServerURL + API.INQUTRY_FEEDBACK);

        SharedPreferences authData = App.context.getSharedPreferences(App.AUTH_DATA, MODE_PRIVATE);
        Gson gson = new Gson();
        UserBean userBean = gson.fromJson(authData.getString(App.LOGIN_DATA, ""), UserBean.class);
        //Log.d(TAG, "JWT token: " + userBean.getJwt_token());

        AndroidNetworking.post(API.debugServerURL + API.INQUTRY_FEEDBACK)
                .addHeaders("Authorization", "JWT " + userBean.getJwt_token())
                .addJSONObjectBody(jsonRequest)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response:" + response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getResponse());
                    }
                });
    }
}